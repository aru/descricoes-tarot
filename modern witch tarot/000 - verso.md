o verso das cartas tem fundo branco com desenho em azul. uma moldura maior azul toma a maior parte do espaço com os dois cantos com uma mão segurando um pentáculo e os outros dois opostos com uma mão segurando um celular que mostra na sua tela um cristal com fones pendurados.

bem no meio da carta tem um olhos que é trespassado por um par de espadas em formato de cruz. acima cima dele há um W de bruxa em inglês (witch) e acima do W um cális com duas chaves. o W, cálice e Taças se repetem espelhados na outra direção do desenho. ao lado do olho há dois galhos com algumas pequenas folhas.
