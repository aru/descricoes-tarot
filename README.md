# descricoes-tarot

descrições de cartas de tarô que eu uso pros meus conteúdos.

# por que?

descrições são importantes por vários motivos mas o mais importante de todos é acessibilidade. eu pretendo descrever todas as cartas dos baralhos de tarô que uso e talvez isso possa ser útil para alguém também então por isso eu to criando o repositório.

# baralhos

cada baralho vai estar em uma pasta separada.

- modern witch tarot: o baralho que eu to mais usando agora e que provavelmente por isso vai ser qual vai ter atualizações mais frequentes
- waiter-smith: o outro baralho que eu uso. vou ir descrevendo ele de vez enquanto

# metodologia

## fuck gender

eu vou usar linguagem 100% sem marcação de gênero. isso é proposital. eu acredito que gênero tenha muito pouco a ver com tarô então eu não vou me esforçar pra fazer interpretações sobre o gênero das figuras que aparecem nas cartas. pra mim é irrelevante.

ainda vou descrever as pessoas que aparecem e os traços que elas tem, mas como eu acabei de fazer nessa frase, eu vou usar ela/a pra muita coisa por que eu vou estar concordando com pessoa/criatura e palavras similares que são parecidas

## o essencial

não é importante descrever absolutamente todos os elementos que aparecem em todas as cartas. as vezes encher a descrição de detalhes pode mais atrapalhar do que ajudar na compreensão do que a ilustração está tentando fazer então, geralmente, eu vou acabar não descrevendo coisas que eu não acho que sejam muito relevantes.

## numeração

as cartas vão ser numeradas de acordo com uma notação que eu tirei da minha cabeça. não acredito que seja nada único ou original, mas eu precisava de uma notação que fosse fácil pra escrever/pesquisar depois que carta é cada uma então eu criei isso. agora que eu tenho esse repositório, vou usar a mesma coisa pra que teoricamente alguém possa ir e achar a carta direto logo.

### o formato
as notações são sempre três números, sendo o primeiro representando o naipe da carta, na ordem "padrão" e os outros dois a posição dela dentro do naipe.
- primeiro número:
	- 0: arcano maior
	- 1: paus
	- 2: copas
	- 3: espadas
	- 4: pentáculos/ouros
- os outros numéros são a posição da carta dentro do naipe.

### exemplos:
- 000 - o verso da carta. excessão
- 001 - a carta do tolo. sim, é 1 e não 0 me processa.
- 113 - rainha de paus
- 202 - dois de copas
- 311 - valete de espadas
- 408 - oito de pentáculos
- assim por diante

mais tarde eu faço a lista completa de cartas e coloco no repositório pra facilitar a consulta. eu mesma não tenho ideia 90% do tempo qual é o número das cartas e consulto o tempo todo :P

### mas pra que isso caraca?

como eu falei antes, assim fica bem fácil de eu anotar num texto quais cartas apareceram tanto pra eu olhar depois em busca de padrões quanto enquanto eu faço as leituras. especialmente por que na minha cabeça as cartas/naipes tem tudo nomes diferentes que nem sempre fazem muito sentidos (espeto, vara, pentaco, pexera, facão, za warudo, pião do bau, cavalo, caneço, taça, goró e a lista segue infinitamente) dai é mais fácil eu anotar o número e não ter que ficar pensando em qual nome eu decidi usar pra carta na hora. facilita

cê também pode usar pra alguma coisa com numerologia se tu quiser mas eu realmente não tenho a mínima noção de anda disso então fica só a sugestão.

ah, também ajuda bastante a organizar uma lista já que dai eu só preciso colocar em ordem alfanumérica e todas as cartas vão parar nos seus devidos lugares, vide esse repositório.

## sem imagens

por motivo de direitos autorais e essas coisas, esse repositório nunca vai ter imagens de nenhuma carta e apenas as descrições.

## língua

as descrições vão ser em português... eu acho. a não ser que por algum motivo eu produza algum conteúdo em alguma outra língua... provavelmente vou acabar traduzindo alguma coisa pra inglês pra fazer alguma postagem em algum lugar e posso acabar colocando em algum lugar aqui.

## conclusão

acho que isso tudo é pra dizer que invariavelmente as minhas descrições vão ser enviesadas por vários motivos e... não tem muito o que fazer. eu vou estar sempre disposta a ouvir sugestões e correções das pessoas mas como eu imagino que eu vá fazer praticamente todo o trabalho, isso será inevitável, então mantenha isso em mente.

# licença

ainda não fui atrás de uma licença mas alguma coisa que não possa usar comercialmente sem autorização mas que permita qualquer pessoa que quer usar pra tornar seus trabalhos mais acessíveis e etc.

# DESCREVE UM BARALHO PRA MIM

me paga que eu descrevo. sério. entra em contato e a gente arruma alguma coisa.

# disclaimer

eu não tenho nenhuma conexão com nenhuma das pessoas que produziu e nem publica nenhum dos baralhos e etc

# autora

oi, eu sou aru. sou uma mulher trans não binária cartomante brasileira que gosta de usar git e sistemas pra tudo e por isso estou aqui. risos.
